# KeyKata

A workout for your fingers,

courtesy of Free and Open Source Software (FOSS).

What skills can I learn here?
1. UNIX Shell Scripting
2. Python Programming
3. R Programming
4. Git Version Control

What keyboard shortcuts are covered?
1. Shell (GNU emacs/readline)
2. Vim

Where can I use these shortcuts?
1. Shell (Bash or Zsh)
2. Web browser (Vimium and SurfingKeys)
3. Code Editor (PyCharm, RStudio, or VSCode)
4. Python or R console (IPython, ptpython, or radian)
5. Jupyter notebook (JupyterLab or classic interface)

## Requirements
- asciinema: to play `.cast` files
- ffmpeg: to play audio files (`.mp3`, `.aac`, or `.ogg`)
- tmux: to run scripts that split the terminal into windows
- miniconda: to install the above along with python and R
